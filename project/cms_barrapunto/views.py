from django.http import HttpResponse,HttpResponseBadRequest,HttpResponseNotFound
from .models import Page
from django.views.decorators.csrf import csrf_exempt # para que no puedan cambiar mi base de datos con un POST
from xml.sax.handler import ContentHandler #clase general de reconocedores de sax
from xml.sax import make_parser
import urllib.request

formulario = """
	<form action="" method="POST">
		Introduzca contenido:<br>
		<input type="text" name="pagina"><br/> 
		<input type="submit" value="Enviar">
	</form>
"""
link=""

html_template="""<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>Django CMS-Barrapunto</title>
	</head>
	<body>
		{content}
	</body>
</html>
"""

html_item_template="<li><a href='{name}'>{name}</a></li>"

def tokenize(url):

	class CounterHandler(ContentHandler):

		def __init__ (self):
			self.inContent = False 
			self.inItem=False  
			self.theContent = ""

		def startElement (self, name, attrs): 
			if name == 'item': 
				self.inItem=True 
			elif self.inItem:    
				if name == 'title': 
					self.inContent = True
				elif name == 'link': 
					self.inContent = True

		def endElement (self, name):
			global link #links
			if self.inItem:
				if name=="title":
					self.title=self.theContent
				elif name=="link":
					self.link=self.theContent
					link+=('<li>TITLE: <a href="'+ str(self.link) + '">' + str(self.title) + '</a></li>' + '\n') 
				elif name=="item":
					self.inItem=False  

				if self.inContent:
					self.inContent=False
					self.theContent=""                 

		def characters (self, chars):    
			if self.inContent:
				self.theContent = self.theContent + chars #meto lo leido

	BarrapuntoParser = make_parser() 
	BarrapuntoHandler = CounterHandler() 
	BarrapuntoParser.setContentHandler(BarrapuntoHandler) 

	xmlFile=urllib.request.urlopen(url)
	BarrapuntoParser.parse(xmlFile)

	return link #devuelve los titulares

List=tokenize("http://barrapunto.com/index.rss")

def index(request):
	pages=Page.objects.all() #muestro las paginas introducidas 
	if len(pages)==0: #no hay ninguna metida todavía 
		respuesta="No hay paginas introducidas todavia.<br>"
	else: #ya hay alguna pagina 
		respuesta="<ul>"
		for p in pages:
			respuesta+=html_item_template.format(name=p.name) #relleno la plantilla
		respuesta+="</ul>"
	
	response= respuesta + "<br>"+ "<h1>Titulares de barrapunto: </h1>" + List + "<br>"	
	html=html_template.format(content="<h1>Lista de paginas introducidas</h1>"+response)
	return HttpResponse(html)
		
@csrf_exempt
def name(request,name):
	if request.method=='GET': #cuando ponemos el /nombre de la pagina en la url 
		try:
			p=Page.objects.get(name=name) #miro si está la pagina pedida 
			content=p.content #me quedo con el contenido
			respuesta=formulario + content #como la pagina ya estaba introducida ahora lo que hago es mostrar el contenido que tenia de antes y el formulario para poder actualizar el contenido de esta pagina si asi lo quisiera
			return HttpResponse(respuesta)
		except Page.DoesNotExist: # si no existe el /nombre de la pagina en la url le paso el formulario para que pueda añadir contenido a esa pagina nueva 
			respuesta=HttpResponseNotFound("Pagina " + name + " no encontrada" + formulario)
			return(respuesta)

	elif request.method=='POST':
		nuevo_contenido=request.POST['pagina'] #me quedo con lo introducido en el formulario
		try: #TODOO LO QUE HAY EN EL TRY ES PARA ACTUALIZAR EL CONTENIDO DE UNA PÁGINA YA EXISTENTE 
			p=Page.objects.get(name=name) #compruebo que esté el nombre de la página 
			p.content=nuevo_contenido # como la página ya estaba introducida le puedo modificar el contenido del formulario
			p.save() #guardo en la base de datos el nuevo contenido introducido 
		except Page.DoesNotExist: # si la página no existe añado el nombre y el contenido que introduje en el formulario 
			nuevo_contenido=Page(name=name,content=nuevo_contenido)
			nuevo_contenido.save() #guardo la nueva pagina entera 
		return HttpResponse("<h1>Dato introducido correctamente!!</h1>")
	else:
		return HttpResponseNotFound("<h1>Metodo no encontrado</h1>")




