from django.apps import AppConfig


class CmsBarrapuntoConfig(AppConfig):
    name = 'cms_barrapunto'
